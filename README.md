# athenaeum

[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/dingseguin/athenaeum.svg)](https://bitbucket.org/dingseguin/athenaeum/addon/pipelines/home)
[![Dashboard athenaeum](https://img.shields.io/badge/dashboard-athenaeum-yellow.svg)](https://dashboard.pantheon.io/sites/acb07aa3-0ddf-4531-a8bf-f40806673d3f#dev/code)
[![Dev Site athenaeum](https://img.shields.io/badge/site-athenaeum-blue.svg)](http://dev-athenaeum.pantheonsite.io/)