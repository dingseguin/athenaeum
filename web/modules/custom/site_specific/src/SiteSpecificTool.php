<?php

namespace Drupal\site_specific;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Our class SiteSpecificTool.
 */
class SiteSpecificTool {

  /**
   * Will represent the Entity Type Manager Interface object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Ourt class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Return the EntityTypeManagerInterface object.
   */
  public function entityTypeManager() {
    return $this->entityTypeManager;
  }

}
