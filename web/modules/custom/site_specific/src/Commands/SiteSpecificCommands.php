<?php

namespace Drupal\site_specific\Commands;
use Drush\Commands\DrushCommands;

/**
 * Provides drush commands to run Site Specific tasks.
 *
 * @package Drush\Commands
 */
class SiteSpecificCommands extends DrushCommands {

  /**
   * Our helper tool.
   */
  protected $tool;

  /**
   * Construct method.
   */
  public function __construct() {
    $this->tool = \Drupal::service('site_specific.tool');
  }

  /**
   * Drush command for updating the question score.
   *
   * @command site_specific:question_score_update
   * @aliases ssqsu
   * @usage site_specific:question_score_update
   * @usage ssqsu
   */
  public function updateQuizScores() {
    $multichoices = $this->tool->entityTypeManager()
      ->getStorage('quiz_question')
      ->loadByProperties(['type' => 'multichoice']);

    if (!empty($multichoices)) {
      foreach ($multichoices as $multichoice) {
        $alternatives = $multichoice->get('alternatives')->referencedEntities();

        if (empty($alternatives)) {
          continue;
        }

        foreach ($alternatives as $alternative) {
          $correct = $alternative->get('multichoice_correct')->value;

          if ($correct == '1') {
            $alternative->set('multichoice_score_chosen', 1);
            $alternative->save();
            $multichoice->save();
          }
        }
      }
    }
  }

}
